use crate::token::{lookup_identifier, Token};

use std::iter::Peekable;
use std::str::Chars;

fn is_letter(ch: char) -> bool {
    ch.is_alphabetic() || ch == '_'
}

fn is_number(ch: char) -> bool {
    ch.is_numeric()
}

fn is_whitespace(ch: char) -> bool {
    ch.is_whitespace()
}

pub struct Lexer<'input> {
    // The string to parse.
    input: &'input str,

    // The starting index of the next character.
    index: usize,

    // The iterator to extract characters from the input.
    chars: Peekable<Chars<'input>>,
}

impl<'input> Lexer<'input> {
    /// Creates a new lexer for the given input.
    pub fn new(input: &'input str) -> Lexer {
        Lexer {
            input,
            index: 0,
            chars: input.chars().peekable(),
        }
    }

    /// Returns the next character and updates the index.
    fn next_char(&mut self) -> Option<char> {
        let ch = self.chars.next();

        if let Some(c) = ch {
            self.index += c.len_utf8();
        }
        ch
    }

    /// Reads a batch of characters so long as the predicate yields true.
    fn select<F>(&mut self, predicate: F) -> &'input str
    where
        F: Fn(char) -> bool,
    {
        let index = self.index;
        while let Some(&ch) = self.chars.peek() {
            if !predicate(ch) {
                break;
            }
            self.next_char();
        }
        &self.input[index..self.index]
    }
}

impl<'input> Iterator for Lexer<'input> {
    type Item = Token<'input>;

    fn next(&mut self) -> Option<Self::Item> {
        self.select(is_whitespace);

        let ch = self.chars.peek().cloned()?;
        Some(if is_letter(ch) {
            let ident = self.select(is_letter);
            lookup_identifier(ident)
        } else if is_number(ch) {
            let number = self
                .select(is_number)
                .parse::<i32>()
                .expect("Error parsing an integer");
            Token::Int(number)
        } else {
            // at this point we have to advance the iterator
            let ch = self.next_char().unwrap();
            match ch {
                '=' => {
                    if self.chars.peek() == Some(&'=') {
                        self.next_char();
                        Token::Eq
                    } else {
                        Token::Assign
                    }
                }
                '+' => Token::Plus,
                '-' => Token::Minus,
                '!' => {
                    if self.chars.peek() == Some(&'=') {
                        self.next_char();
                        Token::NotEq
                    } else {
                        Token::Bang
                    }
                }
                '*' => Token::Asterisk,
                '/' => Token::Slash,
                '<' => Token::Lt,
                '>' => Token::Gt,
                ',' => Token::Comma,
                ';' => Token::Semicolon,
                '(' => Token::Lparen,
                ')' => Token::Rparen,
                '{' => Token::Lbrace,
                '}' => Token::Rbrace,
                _ => Token::Illegal(ch),
            }
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn next_token() {
        let input = "
        let five = 5;
        let ten = 10;

        let add = fn(x, y) {
           x + y;
        };

        let result = add(five, ten);
        !-/*5;
        5 < 10 > 5;

        if (5 < 10) {
            return true;
        } else {
            return false;
        }

        10 == 10;
        10 != 9;
        ";

        assert_eq!(
            Lexer::new(input).collect::<Vec<_>>(),
            &[
                Token::Let,
                Token::Ident("five"),
                Token::Assign,
                Token::Int(5),
                Token::Semicolon,
                Token::Let,
                Token::Ident("ten"),
                Token::Assign,
                Token::Int(10),
                Token::Semicolon,
                Token::Let,
                Token::Ident("add"),
                Token::Assign,
                Token::Function,
                Token::Lparen,
                Token::Ident("x"),
                Token::Comma,
                Token::Ident("y"),
                Token::Rparen,
                Token::Lbrace,
                Token::Ident("x"),
                Token::Plus,
                Token::Ident("y"),
                Token::Semicolon,
                Token::Rbrace,
                Token::Semicolon,
                Token::Let,
                Token::Ident("result"),
                Token::Assign,
                Token::Ident("add"),
                Token::Lparen,
                Token::Ident("five"),
                Token::Comma,
                Token::Ident("ten"),
                Token::Rparen,
                Token::Semicolon,
                Token::Bang,
                Token::Minus,
                Token::Slash,
                Token::Asterisk,
                Token::Int(5),
                Token::Semicolon,
                Token::Int(5),
                Token::Lt,
                Token::Int(10),
                Token::Gt,
                Token::Int(5),
                Token::Semicolon,
                Token::If,
                Token::Lparen,
                Token::Int(5),
                Token::Lt,
                Token::Int(10),
                Token::Rparen,
                Token::Lbrace,
                Token::Return,
                Token::True,
                Token::Semicolon,
                Token::Rbrace,
                Token::Else,
                Token::Lbrace,
                Token::Return,
                Token::False,
                Token::Semicolon,
                Token::Rbrace,
                Token::Int(10),
                Token::Eq,
                Token::Int(10),
                Token::Semicolon,
                Token::Int(10),
                Token::NotEq,
                Token::Int(9),
                Token::Semicolon
            ] as &[_]
        )
    }
}
