mod ast;
mod lexer;
mod parser;
mod repl;
mod token;

use repl::start as start_repl;

fn main() {
    start_repl();
}
